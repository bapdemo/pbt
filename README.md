
# Property based testing with fast-check

<https://github.com/dubzzz/fast-check>

## How to run

1. install latest `node` (for windows users: e.g. by using `scoop`)
2. [Install `yarn`](https://yarnpkg.com/getting-started/install)
2. `cd <directory of code>`
3. `yarn install --immutable`
4. `yarn run test`

## Assignment

There is a [unconventional list implementation](./list.js) that needs to be tested _pbt style_. The code is documented, example calls are included in the source as well. The code is _intentionally_ written in an obscure way: focus on the tests, not on the implementation.

Add your tests to [the list's tests suite](./list.spec.js). You may run the tests with
```
yarn run test-list
```
(or alternatively with
```
yarn exec mocha list.spec.js
```
)

The test suite also already contains some _unit_ tests, that demonstrate how the _list_ is to be used.

There is already an _arbitrary_ for lists `arbitraryList()` which can be used to write your properties.

See [the slides](./SLIDES.md) for details, which tests shall be implemented.
Note the corresponding idea/approach from the slides in the test case name.

A simple test might look like this:

```
describe("PBT Tests for lists", () => { // test category
  it("Should trivially append with empty", () => { // test case
    fc.assert(
      fc.property(
        arbitraryList(fc.nat()), // for an arbitrary list of natural numbers
        (l) => eq(l, append(empty, l))) // there is no change when the empty list is appended
    );
  });
});
```

When done, push a branch `pbt` to your hand-in repository.
