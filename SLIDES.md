
# Property Based Testing

## Motivation

### Automatic Testing

- There are several flavors of automatic tests.
- We assume, you know already lots about _unit testing_
- You may even written some _integration_ test

. . .

_Property based testing_ is another _flavor_ of automatic testing.

. . .

It is meant to _complement_ the other flavors - __not__ to _replace_ them.

. . .

Pick the _right_ tool for the job at hand.

## Introducing our example

### Singly linked list

```
l = empty
```
constructs an _empty list_ `l`

. . .

```
al = cons(a, l)
```
prepends `a` to the list `l` to make a new list `al` (`l` remains unchanged)

### Singly linked list (contd.)

You can deconstruct a list with `first` and `rest`

```
al = cons(a, l)
a === first(al)
l === rest(al)
```

. . .

This of course fails if a list is _empty_

```
l = empty
true === isEmpty(l)
```

### Singly linked list (contd.)

- `eq` to compare two lists
- `length` counts the elements in a list
- `append` to append _two lists_ (note: `cons` takes an _element_ and a list)
- `reverse` reverses a list
- `find` traverses the list in search of a particular element
- `map` transforms a list element-wise
- `filter` filters a list element-wise
- `reduce` combines all elements of a list - one element at a time
- `reduceRight` the same, but starting with the right-most element (i.e. right to left)

### Check the code

The implementation is quite _unusual_. Have a look for yourself [`list.js`](./list.js).

Can you figure out how it works?

## Property based testing

### fast-check

We utilize the [fast-check](https://github.com/dubzzz/fast-check) library (in conjunction with the test runner [mocha](https://mochajs.org)) to do _property based testing_ in JavaScript.

There probably exists a library for the programming language of your choice.

. . .

_Property based testing_ is a _technique_. It can be implemented in principle in any language.

### Properties

In contrast to classic unit testing, where a set of test data is predetermined, to be used to run the code - you need to formulate _truthful properties_, which will be verified with arbitrary test data.

To give you some idea, how such properties could look like, we have already written some tests, which test basic arithmetic capabilities of JavaScript. Try to use them as inspiration to write similar tests for the list.

### Properties (contd.)

In general a property looks like this

- _For all_ `x`, `y`, `z`, ... _being of some kind_ (e.g. _for all_ `x` _being an integer_)
- such that a _precondition_ holds (e.g. _greater than 0_) (there can also be _no_ precondition)
- property `p(x, y, z, ...)` is `true` (e.g. `x + y + z + z + z == 3 * z + y + x`)

### Properties (contd.)

With _mocha_ and _fast-check_ this reads

```
describe('Test category name', () => {
  it('Test case name', () => {
    fc.assert(
      fc.property(
        fc.integer(), fc.integer(), fc.integer(),
        (x, y, z) => x + y + z + z + z === 3 * z + y + x))
  })
  // add more `it`s here
})
// add more `describe`s here
```

### Arbitraries

`fc.integer` is called an _arbitrary_ (since it represents an _arbitrary_ integer).
_fast-check_ comes with [lots of built-in arbitraries](https://github.com/dubzzz/fast-check/blob/master/documentation/Arbitraries.md).

You can preview some arbitrary values in the REPL:

```
$ node
> const fc = require('fast-check')
> fc.sample(fc.integer(), 10)
```

### `fc.assert`

Can you guess what it does?

## Coming up with some properties

### The lazy programmer

This section borrows heavily from [Scott Wlaschin's talk "The lazy programmer's guide to writing 1000's of tests"](https://fsharpforfunandprofit.com/pbt/). I recommend that you have a look and a listen.

### Trivial identities

Try to find some identities concerning _trivial_ values (i.e. the simplest values imaginable).
E.g.

- `a + 0 === a`
- `a * 1 === a`
- `a / 1 === a`
- `a * 0 === 0`

Find and code **at least 2** such identities.

### Different paths, same destination

Try to find separate ways, to come to the same result.
E.g.

- `a + b === b + a`
- `a + (b + c) === (a + b) + c`

Think of (and then code) **at least 1** such property.

### There and back again

Try to operations that cancel themselves.

- `a * (-1) * (-1) === a`
- `(a * b) / b === a`

Think of (and then code) **at least 1** such property

Hint: if you run in some subtleties you may enforce some stronger guarantees by using [preconditions](https://github.com/dubzzz/fast-check/blob/master/documentation/Tips.md#filter-invalid-combinations-using-pre-conditions)

### Some things never change

A.k.a. _invariants_. Find an operation and a corresponding aspect, such that no matter, how often the operation is applied, the aspect never changes.

- `a === a * a [mod 2]` i.e. the parity of a number is preserved when they are squared

Think of (and then code) **at least 1** such property

### The more things change, the more they stay the same

Applying on operation more often somehow leads to predictive results

- `abs(a) === abs(abs(a))`
- `a & b = a & b & b`

Think of (and then code) **at least 1** such property

### Solve a smaller problem first

Try to simplify an operation at hand, and then reconstruct the solution of the original operation. Maybe splitting an operation into two simpler ones can help find such properties.

- `a * b === (a * (b-1)) + a` e.g. `12 * 11` can first be simplified to `12 * 10` which obviously is `120` and then adding the last `12` results in `132`
- `a * 8 === a * 2 * 2 * 2` the idea being, if you _combine_ the _doubling_ three times, you get the same result as multiplying by 8 _directly_

Think of (and then code) **at least 1** such property

### Hard to prove, easy to verify

If your system under test generates some artifact, where its correctness can be easily verified go for it

- `product(...primeFactors(a)) === a` e.g. `primeFactors` is the system under test. It is very easy to calculate the product, which should equal the original input.

I suspect there is no obvious application for our list here, but feel free to try.

### The test oracle

If somehow you have an _oracle_ which knows the correct solution, you might as well use it. E.g. if you have a reference implementation, you could feed the same input to the reference implementation and compare the results.

- `f(a) === oracleForF(a)`
- `newCode(a) === oldSystemToBeReplaced(a)`

I suspect there is no obvious application for our list here, but feel free to try.

## Practice

### Write some properties for `list`

Pick _at least_ 2 approaches to come up with properties for the list.

And implement the suggested number of properties.

Have a look at [list.spec.js](./list.spec.js). There is already an _arbitrary_ for lists in there:

```
arbitraryList(fc.string())
```

Is an arbitrary list of strings.
